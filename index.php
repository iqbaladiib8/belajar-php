<?php

require_once('animal.php');
require_once('ape.php');
require_once('frog.php');

$sheep = new Animal("shaun");

echo "Name  : ".$sheep->name."<br>"; // "shaun"
echo "Legs  : ".$sheep->legs."<br>"; // 4
echo "Cold Blooded  :  ".$sheep->cold_blooded."<br>"; // "no"
echo "<br><br>";

$sungokong = new Ape("Kera Sakti");

echo "Name  : ".$sungokong->name."<br>"; // "kera sakti"
echo "Legs  : ".$sungokong->legs."<br>"; // 4
echo "Cold Blooded  :  ".$sungokong->cold_blooded."<br>"; // "no"
echo $sungokong -> yell(); //Auooo
echo "<br><br>";

$kodok = new Frog("Buduk");

echo "Name  : ".$kodok->name."<br>"; // "BUduk"
echo "Legs  : ".$kodok->legs."<br>"; // 4
echo "Cold Blooded  :  ".$kodok->cold_blooded."<br>"; // "no"
echo $kodok -> jump(); //Hop Hop
echo "<br><br>";

?>